import {PluginType} from '../types/PluginType'

let doclinks = new Map<PluginType,String>();
doclinks.set(PluginType.GITHUB_APP,'https://docs.paloaltonetworks.com/prisma/prisma-cloud/prisma-cloud-admin/prisma-cloud-devops-security/use-the-prisma-cloud-app-for-github.html');
doclinks.set(PluginType.VSCODE_PLUGIN,'https://docs.paloaltonetworks.com/prisma/prisma-cloud/prisma-cloud-admin/prisma-cloud-devops-security/use-the-prisma-cloud-extension-for-vs-code.html');
doclinks.set(PluginType.AZURE_DEVOPS,'https://docs.paloaltonetworks.com/prisma/prisma-cloud/prisma-cloud-admin/prisma-cloud-devops-security/use-the-prisma-cloud-extension-for-azure-devops.html');

export default doclinks;
