import * as rm from 'typed-rest-client/RestClient'
import * as ht from 'typed-rest-client/HttpClient'
import * as https from "https";
export class TypedRestClient
{
    private static readonly rest = new rm.RestClient(undefined,undefined,undefined ,{ignoreSslError:true});
    private static readonly http = new ht.HttpClient(undefined,undefined,{ignoreSslError:true})
    private constructor()
    {}

    public static getRestClient() : rm.RestClient
    {
        return TypedRestClient.rest;
    }

    public static getHttpClient(): ht.HttpClient
    {
        return TypedRestClient.http;
    }


}
