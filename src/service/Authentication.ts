import { Error, PrismaCloudConfig } from "../types";
import { PluginType } from "../types/PluginType";


export interface Authentication
{
    getAuthenticationToken(pcsConfig: PrismaCloudConfig, pluginType: PluginType): Promise<PrismaCloudConfig|Error>;

}