import { Error, ErrorCode, ErrorMap, PrismaCloudConfig } from "../types";
import { Authentication } from "./Authentication";
import * as rm from "typed-rest-client/RestClient"
import { TypedRestClient } from "../httpclient";
import { PluginType } from "../types/PluginType";
import doclinks from "../dict/doclinks";
import jwt_decode, { JwtPayload } from "jwt-decode";


export class AuthenticationService implements Authentication
{
    async getAuthenticationToken(pcsConfig: PrismaCloudConfig, pluginType: PluginType): Promise<PrismaCloudConfig|Error> {
    
    if(pcsConfig.getExpiryTime() == undefined || tokenExpiredOrUnavialable(pcsConfig.getExpiryTime()))
    {
    const rc : rm.RestClient = TypedRestClient.getRestClient();
     let _options = {
        additionalHeaders: {
          'Content-Type': 'application/json'
        },
        acceptHeader : 'application/json',

    }

    let body = {
      username: pcsConfig.getAccessKey(),
      password: pcsConfig.getSecretKey()
    }
    let rs : rm.IRestResponse<ResultType>;
    try{
      rs = await rc.create(pcsConfig.getUrl() + '/login',body, _options);
     
      if((rs.statusCode === 200 || rs.statusCode === 201) && rs.result.token)
      {
            const decodedToken = jwt_decode<JwtPayload>(rs.result.token);
            pcsConfig.setToken(rs.result.token);
            pcsConfig.setExpiryTime(decodedToken.exp);
            console.log('JWT token recieved Successfully')
            return Promise.resolve(pcsConfig);
      }
      // in some case the library throws exception and in some errors it adds the result in the json without throwing exception
      else 
      {
      console.error('Failed to get token. summary: ' + rs)
      let message : String = ErrorMap[ErrorCode[ErrorCode.INTERNAL_ERROR]].value + doclinks.get(pluginType)
      return Promise.reject(new Error(message,ErrorCode[ErrorCode.INTERNAL_ERROR],rs.statusCode,rs.result));
    } 
  }
    catch(err)
    {
      console.error('Failed to get token. summary: ' + err)
      if(err['statusCode'] === 400 || err['statusCode'] === 401)
      {
        let message : String = ErrorMap[(ErrorCode[ErrorCode.INVALID_CREDENTIALS])].value + doclinks.get(pluginType)
        return Promise.reject(new Error(message,ErrorCode[ErrorCode.INVALID_CREDENTIALS],err.statusCode,err.result));
  
      }
      else 
      {
        let message : String = ErrorMap[ErrorCode[ErrorCode.INTERNAL_ERROR]].value + doclinks.get(pluginType)
      return Promise.reject(new Error(message,ErrorCode[ErrorCode.INTERNAL_ERROR],err.statusCode,err.result));

      }
    }
        
    }
    else
    {
    return Promise.resolve(pcsConfig)
    }
  }
  
    
}

function tokenExpiredOrUnavialable(pcsExpiryTime: Number): boolean
{
  let isTokenExpired = false;
  if(pcsExpiryTime && pcsExpiryTime != undefined)
  {
  const now = new Date()
  if (pcsExpiryTime <= Math.round(now.getTime() / 1000)) {
    isTokenExpired = true
  }
  }
  else{
    isTokenExpired = true;
  }

  return isTokenExpired;
}


class ResultType{
  token?: string
  message?: string
	constructor(token?: string,message?: string) {
    this.token = token;
    this.message = message;
	}
  
  
}