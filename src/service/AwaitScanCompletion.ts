import { PluginType } from "..";
import { Error, ErrorCode, ErrorMap } from "../types";
import { PrismaCloudConfig } from "../types/PrismaCloudConfig";
import { ScanStatus } from "./ScanStatus";
import { ScanStatusService } from "./ScanStatusService";

export async function awaitScanCompletion(startTime:Date, attributes : {}, pcsConfig: PrismaCloudConfig, pluginType: PluginType, headers?:{}, waitTimeinMilliseconds : number = 2000)
{
    let scanStatus : ScanStatus = new ScanStatusService();
    let maxWaitTime;
    if(startTime != undefined && startTime != null) {
        maxWaitTime = startTime.getTime() + (15 * 60 * 1000)
    }

    while(true)
    {
        if (startTime != undefined && startTime != null && maxWaitTime <= (new Date().getTime() + 3000)) {
            let message : String = ErrorMap[ErrorCode[ErrorCode.TIME_OUT]].value + attributes['id']
            console.log("Request timed out. Please try again.")
            return Promise.reject(new Error(message,ErrorCode[ErrorCode.TIME_OUT]));
          }
       // wait for timeout
    await new Promise(resolve => setTimeout(resolve, waitTimeinMilliseconds));
    
    try{
     let scanStatusResult  = await scanStatus.getScanStatus(attributes, pcsConfig, pluginType, headers)
     if(scanStatusResult.data.attributes.status != 'processing')
     {
         return Promise.resolve(scanStatusResult);
     }
    }
    catch(err)
    {
        return Promise.reject(err)
    }
    }
    


}