import { PluginType } from "../types/PluginType";
import { PrismaCloudConfig } from "../types/PrismaCloudConfig";

export interface GetScanResults{
    getScanResults(attributes : {}, pcsConfig: PrismaCloudConfig, pluginType: PluginType, headers?:{})
}