import { TypedRestClient } from "../httpclient/TypedRestClient";
import { PluginType } from "../types/PluginType";
import { PrismaCloudConfig } from "../types/PrismaCloudConfig";
import { AuthenticationService } from "./AuthenticationService";
import * as rm from "typed-rest-client/RestClient"

import { ErrorCode, ErrorMap,Error } from "../types/Error";
import doclinks from "../dict/doclinks";
import { GetScanResults } from "./GetScanResults";
export class GetScanResultsMarkdownService implements GetScanResults
{
    async getScanResults(attributes : {}, pcsConfig: PrismaCloudConfig, pluginType: PluginType, headers?:{})
    {
        let authTokenPromise =  new AuthenticationService().getAuthenticationToken(pcsConfig,pluginType);
        authTokenPromise.catch(error => {return Promise.reject(error)})


        const rc : rm.RestClient = TypedRestClient.getRestClient();
        let moreHeaders =  {
            'x-redlock-auth': pcsConfig.getToken()
        }
        let additionalHeaders = Object.assign({}, moreHeaders, headers);
        let _options = {
            additionalHeaders: additionalHeaders,
            acceptHeader : 'application/vnd.api+json',
        }

        let rs : rm.IRestResponse<any>;
        try{
            rs = await rc.get(pcsConfig.getUrl() + '/iac/v2/scans/'+attributes['id']+'/results/markdown', _options);

            if((rs.statusCode === 200 || rs.statusCode === 201))
            {
                console.log('SuccessFully Got Scan results in markdown format')
                return Promise.resolve(rs.result)
            }
            // in some case the library throws exception and in some errors it adds the result in the json without throwing exception
            else
            {
                console.error('Failed to Get scan results ' + rs)
                let message : String = ErrorMap[(ErrorCode[ErrorCode.INTERNAL_ERROR])].value + doclinks.get(pluginType)
                return Promise.reject(new Error(message,ErrorCode[ErrorCode.INTERNAL_ERROR],rs.statusCode,rs.result));
            }
        }
        catch(err)
        {
            console.error('Failed to Get Scan results ' + err)
            if(err['statusCode'] === 400 || err['statusCode'] === 401)
            {
                let message : String = ErrorMap[(ErrorCode[ErrorCode.BAD_REQUEST])].value + doclinks.get(pluginType)
                return Promise.reject(new Error(message,ErrorCode[ErrorCode.BAD_REQUEST],err.statusCode,err.result));

            }
            else
            {
                let message : String = ErrorMap[(ErrorCode[ErrorCode.INTERNAL_ERROR])].value + doclinks.get(pluginType)
                return Promise.reject(new Error(message,ErrorCode[ErrorCode.INTERNAL_ERROR],err.statusCode,err.result));
            }
        }

    }
}
