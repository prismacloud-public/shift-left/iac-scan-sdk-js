import {AuthenticationService, GetScanResultsMarkdownService, PluginType, PrismaCloudConfig} from "..";
import { awaitScanCompletion } from "./AwaitScanCompletion";
import { GetScanResultsService } from "./GetScanResultsService";
import { ScanInitService } from "./ScanInitService";
import { TriggerScanService } from "./TriggerScanService";
import { UploadPayloadService } from "./UploadPayloadService";
import {GetScanResultsSarifService} from "./GetScanResultsSarifService";
import {getVersion} from "../util/Sdkversion";

async function getInitScanBody(scanContext) {

    var initBody = {"data": {
            "type": "async-scan",
            "attributes": {
                "assetName": scanContext.assetName,
                "assetType": scanContext.assetType,
                "tags": scanContext.tags,
                "scanAttributes": scanContext.scanAttributes,
                "failureCriteria": scanContext.failureCriteria
            }
        }
    };
    return initBody;

}

export class IacScanService {
    private prismaCloudConfig: PrismaCloudConfig;


    private pluginType: PluginType;
    private headers: {};
    private initScanRequestData: { initScanBody: {}, initScanHeaders?: {} };


    private uploadDataRequest: { payload: any, headers?: {} };


    private triggerScanRequest: { body: {}, headers?: {} };

    private scanStatusRequest: { headers?: {} } = {headers: {}};

    private scanResultRequest: { headers?: {} } = {headers: {}};


    private startTime: Date;


    public getStartTime(): Date {
        return this.startTime;
    }

    public setStartTime(startTime: Date): void {
        this.startTime = startTime;
    }

    public getPrismaCloudConfig(): PrismaCloudConfig {
        return this.prismaCloudConfig;
    }

    public setPrismaCloudConfig(prismaCloudConfig: PrismaCloudConfig): void {
        this.prismaCloudConfig = prismaCloudConfig;
    }

    public getPluginType(): PluginType {
        return this.pluginType;
    }

    public setPluginType(pluginType: PluginType): void {
        this.pluginType = pluginType;
    }

    public getHeaders(): {} {
        return this.headers;
    }

    public setHeaders(headers: {}): void {
        this.headers = headers;
    }

    public getInitScanRequestData() {
        return this.initScanRequestData;
    }

    public setInitScanRequestData(initScanRequestData): void {
        this.initScanRequestData = initScanRequestData;
    }

    public getUploadDataRequest() {
        return this.uploadDataRequest;
    }

    public setUploadDataRequest(uploadDataRequest): void {
        this.uploadDataRequest = uploadDataRequest;
    }

    public getTriggerScanRequest() {
        return this.triggerScanRequest;
    }

    public setTriggerScanRequest(triggerScanRequest): void {
        this.triggerScanRequest = triggerScanRequest;
    }

    public getScanStatusRequest(): { headers?: {} } {
        return this.scanStatusRequest;
    }

    public setScanStatusRequest(scanStatusRequest: { headers: {} }): void {
        this.scanStatusRequest = scanStatusRequest;
    }

    public getScanResultRequest(): { headers?: {} } {
        return this.scanResultRequest;
    }

    public setScanResultRequest(scanResultRequest: { headers: {} }): void {
        this.scanResultRequest = scanResultRequest;
    }




    async performIacScan(scanContext) {
        getVersion();
        let config : PrismaCloudConfig = new PrismaCloudConfig();
        config.setUrl(scanContext.url)
        config.setAccessKey(scanContext.accessKey)
        config.setSecretKey(scanContext.secretKey)
        this.prismaCloudConfig = config;
        let authService = new AuthenticationService()
        const authTokenResult = await authService.getAuthenticationToken(this.prismaCloudConfig, scanContext.pluginType)
        const initScanBody = await getInitScanBody(scanContext);

        let scanInitService = new ScanInitService();
        const initScanResult = await scanInitService.initScan(initScanBody, this.prismaCloudConfig, scanContext.pluginType, {'User-Agent' : scanContext.userAgent})
        const scanId: string = initScanResult['data']['id']
        console.log("Scan Initialized with scan Id : " + scanId)

        let uploadService = new UploadPayloadService();
        await uploadService.uploadPayload(initScanResult['data']['links']['url'], scanContext.payload, {'User-Agent' : scanContext.userAgent}, scanContext.pluginType)

        let triggerService = new TriggerScanService();

        var iacAttributes = scanContext.iacAttributes
        delete iacAttributes.tags
        const triggerScanResult = await triggerService.triggerScan({data : {id : scanId, attributes: iacAttributes}}, this.prismaCloudConfig, scanContext.pluginType, {'User-Agent' : scanContext.userAgent})

        let scanStatusResult = await awaitScanCompletion(scanContext.startTime, {id: scanId}, this.prismaCloudConfig, scanContext.pluginType, {'User-Agent' : scanContext.userAgent})
        
        return {scanId: scanId, scanStatus: scanStatusResult.data.attributes.status}
    }

    async getScanResultsJsonModel(scanContext) {

        try {
            let scanData = await this.performIacScan(scanContext)
            let getScanResultsService = new GetScanResultsService();

            const scanResults = await getScanResultsService.getScanResults({id: scanData["scanId"]}, this.prismaCloudConfig, scanContext.pluginType, {'User-Agent' : scanContext.userAgent})
            return Promise.resolve(Object.assign({}, scanData, {scanResults:scanResults}));
        } catch (err) {
            console.error('Error occured during scan :' + err.toString())
            return Promise.reject(err)
        }
    }

    async getScanResultsInMarkdownFormat(scanContext) {

        try {
            let scanData = await this.performIacScan(scanContext)
            let getScanResultsService = new GetScanResultsMarkdownService();

            const scanResults = await getScanResultsService.getScanResults({id: scanData["scanId"]}, this.prismaCloudConfig, scanContext.pluginType, {'User-Agent' : scanContext.userAgent})
            return Promise.resolve(scanResults);
        } catch (err) {
            console.error('Error occured during scan :' + err)
            return Promise.reject(err)
        }
    }

    async getScanResultsInSarifFormat(scanContext) {

        try {
            let scanData = await this.performIacScan(scanContext)
            let getScanResultsService = new GetScanResultsSarifService();

            const scanResults = await getScanResultsService.getScanResults({id: scanData["scanId"]}, this.prismaCloudConfig, scanContext.pluginType, {'User-Agent' : scanContext.userAgent})
            return Promise.resolve(scanResults);
        } catch (err) {
            console.error('Error occured during scan :' + err)
            return Promise.reject(err)
        }
    }
}