import * as yaml from "js-yaml";
import {TemplateType} from "../types";



export async function readTemplateConfigFromRepo(content: string)
    {
        let attributes ={}
        try{
        const configYaml = yaml.load(content)
        const jsonString = JSON.stringify(configYaml, null, 4).replace(/template_type/gi, "templateType").replace(/template_version/gi, "templateVersion")
        .replace(/template_parameters/gi, "templateParameters").replace(/variable_files/gi, "variableFiles").replace(/policy_id_filters/gi, "policyIdFilters")
        .replace(/variable_values/gi,"variables");
        let obj = JSON.parse(jsonString);
        for(var key in obj)
        {
                if(obj.hasOwnProperty(key))
                {
                    if(key == "templateType")
                    {
                        attributes[key] = String(obj[key]).toUpperCase()
                    }

                    else if(key == "templateVersion")
                    {
                        attributes[key] = obj[key]
                    }

                    else if(key == "templateParameters")
                    {
                        attributes[key] = obj[key]
                    }

                    else if(key == "terraform_011_parameters")
                    {
                        addVariablesAndVariableFilesToAttributesT11(obj[key], attributes)
                    }
                    else if(key == "terraform_012_parameters")
                    {
                        addVariablesAndVariableFilesToAttributesT12(obj[key], attributes)
                    }
                    else if(key == "cft_parameters")
                    {
                        addVariablesAndVariableFilesToAttributesCFT(obj[key], attributes)
                    }
                    else if(key == 'tags')
                    {
                        addTags(obj[key], attributes)
                    }
                    else{
                        attributes[key] = obj[key]
                        attributes["templateVersion"] = 0.11;
                    }
                }
        }

        if(Object.keys(attributes).length > 0
            && (attributes.hasOwnProperty('templateType'))
            && attributes['templateType'] == TemplateType.TF.valueOf()
            && !(attributes.hasOwnProperty('templateVersion'))  )
        {
            attributes["templateVersion"] = 0.11;
        }
        return attributes
    }
        catch(e)
        {
                console.error('Error reading config yaml' + e);
                throw new Error(e)
        }

    }

function addTags(value:any, attributes:{})
{
     let tags ={}
     if(value != undefined)
     {
         if(Array.isArray(value))
         {
            for(let i = 0; i < value.length; i++) {
                var keyValue = value[i].split(':');
                tags[keyValue[0].trim()] = keyValue.slice(1).join(':').trim();
            }
         }
         else {
            tags = value;
         }
     }
  attributes["tags"] = tags
}

function addVariablesAndVariableFilesToAttributesCFT(value:any, attributes:{})
{
    if(value != undefined)
    {
    const variables = {}
    attributes["templateParameters"] = {}
          
    attributes["templateParameters"]["variables"] = variables;
    if(value.variableFiles !=undefined && Array.isArray(value.variableFiles))
    {
        attributes["templateParameters"]["variableFiles"] = value.variableFiles;
    }

    if (typeof value.variables !== 'undefined' && Array.isArray(value.variables)) {
        const variablesArray = value.variables
        for (let j = 0; j < variablesArray.length; j++) {
          if (typeof variablesArray[j] !== 'undefined') {
            variables[variablesArray[j].name] = variablesArray[j].value
          }
        }
        attributes["templateParameters"]["variables"] = variables;
      }
    }

}

function addVariablesAndVariableFilesToAttributesT12(value:any, attributes: {})
{
    if(value != undefined && Array.isArray(value))
    {
        const variableFiles = []
        const variables = {}
        for (let i = 0; i < value.length; i++) {
            let tf12Param = value[i]
            //ignore root modules.
            if(typeof tf12Param.root_module !== 'undefined' &&  Array.isArray(tf12Param.root_module))
            {
                tf12Param = tf12Param.root_module;
            }
            if (typeof tf12Param.variableFiles !== 'undefined' && Array.isArray(tf12Param.variableFiles) && tf12Param.variableFiles) {
              const varfiles = tf12Param.variableFiles
              for (let j = 0; j < varfiles.length; j++) {
                if (typeof varfiles[j] !== 'undefined' && varfiles[j] != '') {
                  variableFiles.push(varfiles[j])
                }
              }
            }
            if (typeof tf12Param.variables !== 'undefined' && Array.isArray(tf12Param.variables) && tf12Param.variables) {
              const variablesArray = tf12Param.variables
              for (let j = 0; j < variablesArray.length; j++) {
                if (typeof variablesArray[j] !== 'undefined') {
                  variables[variablesArray[j].name] = variablesArray[j].value
                }
              }
            }
          }
          attributes["templateParameters"] = {}
          attributes["templateParameters"]["variableFiles"] = variableFiles;
          attributes["templateParameters"]["variables"] = variables;
    }
}
    

function addVariablesAndVariableFilesToAttributesT11(value:any, attributes: {})
{
    if(value != undefined)
    {
    if (value.variableFiles != undefined) {
        var files = [];
        var varFiles = value.variableFiles;
        for (var j = 0; j < varFiles.length; j++) {
            files.push(varFiles[j]);
        }
        attributes["templateParameters"] = {}
        attributes["templateParameters"]["variableFiles"] = files;
        
    }
    if (value.variables != undefined) {
        var variableValuesTF11 = value.variables;
        var variableValues = {};
        for (var i = 0; i < variableValuesTF11.length; i++) {
            variableValues[variableValuesTF11[i].name] = variableValuesTF11[i].value;
        }
        attributes["templateParameters"]["variables"] = variableValues;
    }
}
}