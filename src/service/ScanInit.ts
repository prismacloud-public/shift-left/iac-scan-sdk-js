import { PluginType } from "../types/PluginType";
import { PrismaCloudConfig } from "../types/PrismaCloudConfig";
import { ScanInitContext } from "../types/ScanInitContext";

export interface ScanInit{
    initScan(jsonApiModel : {}, pcsConfig: PrismaCloudConfig, pluginType: PluginType, headers?:{}): Promise<any>
}