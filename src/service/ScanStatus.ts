import { PluginType } from "../types/PluginType";
import { PrismaCloudConfig } from "../types/PrismaCloudConfig";

export interface ScanStatus
{
    getScanStatus(attributes : {}, pcsConfig: PrismaCloudConfig, pluginType: PluginType, headers?:{}): Promise<any>
}