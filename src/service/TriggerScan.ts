import { PluginType } from "../types/PluginType";
import { PrismaCloudConfig } from "../types/PrismaCloudConfig";

export interface TriggerScan{
    triggerScan(jsonApiModel : {}, pcsConfig: PrismaCloudConfig, pluginType: PluginType, headers?:{}): Promise<any>
}