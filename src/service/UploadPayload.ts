import { PluginType } from "..";

export interface UploadPayload{
    uploadPayload(url: string, payload:any, headers:{},pluginType:PluginType);
}