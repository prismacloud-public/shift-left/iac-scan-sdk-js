import {ErrorCode, ErrorMap, PluginType, PrismaCloudConfig, Error} from "../types";
import {AuthenticationService} from "./AuthenticationService";
import * as rm from "typed-rest-client/RestClient"
import * as ht from "typed-rest-client/HttpClient"

import {TypedRestClient} from "../httpclient/TypedRestClient";
import {ScanInit} from "./ScanInit";
import {UploadPayload} from "./UploadPayload";
import doclinks from "../dict/doclinks";

const rp = require('request-promise')


export class UploadPayloadService implements UploadPayload {
    async uploadPayload(url: string, payload: any, headers: {}, pluginType: PluginType) {


        let additionalHeaders = Object.assign({}, {'Content-Type': 'application/octet-stream'}, headers);

        const options = {
            method: 'PUT',
            uri: url,
            headers: additionalHeaders,
            body: Buffer.from(payload)
        }

        try {
            console.info('Starting s3 upload.....' + payload.length)
            if (payload.length > 0) {
            const response = await rp(options)
            console.log('s3 upload completed.')
            return response
            } else {
                return Promise.reject(new Error("Empty payload, no template found", "EMPTY_INPUT BODY", 400, "BAD_REQUEST"));
            }
        } catch (err) {
            let message: String = ErrorMap[ErrorCode[ErrorCode.INTERNAL_ERROR]].value + doclinks.get(pluginType)
            return Promise.reject(new Error(message, err.name, 500, err.message));
        }

    }
}