import { Error } from "./Error";
import { Status } from "./Status";

export class AuthenticationToken
{
    private token?: String;
    private tokenExpiry?: number
    private error?: Error;

    public getToken(): String {
        return this.token;
    }
    public getError(): Error {
        return this.error;
    }

    constructor($token?: String, $tokenExpiry?: number, $error?: Error) {
        this.token = $token;
        this.tokenExpiry = $tokenExpiry;
		this.error = $error;
	}
}