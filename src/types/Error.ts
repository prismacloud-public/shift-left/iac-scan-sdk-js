export class Error
{
    private code?: number;
    private message: String;
    private name?: string
    private serverMessage?: any

    constructor(message: String, name?:string, code?: number, serverMessage?:any) {
        this.code = code;
        this.name = name;
		this.message = message;
		this.serverMessage = serverMessage;
    }

    public getCode(): number {
        return this.code;
    }

    public getMessage(): String {
        return this.message;
    }

    public getName():string {
        return this.name;
    }

    public getServerMessage():any{
        return this.serverMessage;
    }
    public toString():string
    {
       return 'code =' + this.code + '\n'+
        'name=' + this.name +'\n'+
        'message='+this.message + '\n'+
        'serverMessage='+JSON.stringify(this.serverMessage)+'\n';
}
    
}

export enum ErrorCode
{
    BAD_REQUEST ,//= "Invalid credentials, please verify that API URL, Access Key and Secret Key in your Github repository master branch .github/prisma-cloud-config.yml file are valid. For details refer to: ",
    INTERNAL_ERROR,// = "Oops! Something went wrong, please try again or refer to documentation here: ",
    TIME_OUT ,//= 'Scan took longer than 10 minutes. Please check status on Prism Cloud for ScanID:'
    RESOURCE_NOT_FOUND,
    INVALID_CREDENTIALS,
    UNKOWN_ERROR
}

let ErrorMap: Map<String, { value: string }> = new Map<String, { value: string }>()
ErrorMap[ErrorCode[0]] = {value: 'Bad request, please verify that request body, headers are correct. For details refer to: '}
ErrorMap[ErrorCode[1]] = {value: 'Oops! Something went wrong, please try again or refer to documentation here: '}
ErrorMap[ErrorCode[2]] = {value: 'Scan took longer than 10 minutes. Please check status on Prism Cloud for ScanID:'}
ErrorMap[ErrorCode[3]] = {value: 'Resource Not found'}
ErrorMap[ErrorCode[4]] = {value: 'Invalid credentials, please verify that API URL, Access Key and Secret Key are valid. For details refer to: '}
ErrorMap[ErrorCode[5]] = {value: 'An unknown error occured'}
export {ErrorMap}
