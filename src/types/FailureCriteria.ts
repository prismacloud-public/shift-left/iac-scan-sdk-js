import { FailureCriteriaBuilder } from "./builder/FailureCriteriaBuilder";

export class FailureCriteria
 {
       
    private high: Number
        
    private medium: Number
    
    private low: Number
    
    private operator: String

    public getHigh() {
        return this.high;
    }

    public getMedium() {
        return this.medium;
    }


    public getLow() {
        return this.low;
    }

   
    public getOperator() {
        return this.operator;
    }

    constructor(failureCriteriaBuilder: FailureCriteriaBuilder)
    {
           this.high = failureCriteriaBuilder.high;
           this.medium = failureCriteriaBuilder.medium;
           this.low = failureCriteriaBuilder.low;
           this.operator = failureCriteriaBuilder.operator;
    }


        
 }
