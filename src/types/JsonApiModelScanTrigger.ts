import {TemplateParameters} from "./TemplateParameters"
export interface JsonApiModelScanTrigger {
    data: {
      /**
       * (Optional) {json.api} scan UUID
       */
      id?: string
      attributes: {
        /**
         * IaC template type. Supported types are cft, k8s, tf
         */
        templateType: string
        /**
         * (optional) Template version
         */
        templateVersion?: string
        templateParameters?: TemplateParameters
        
      }
      
    }
  }
  