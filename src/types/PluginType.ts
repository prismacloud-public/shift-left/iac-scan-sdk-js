export enum PluginType
{
    GITHUB_APP,
    GITHUB_ACTION,
    AZURE_DEVOPS,
    VSCODE_PLUGIN
}