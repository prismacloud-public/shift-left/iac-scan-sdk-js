
export class PrismaCloudConfig
{
    private url: String;
    private accessKey?: String;
    private secretKey?: String;
    private token?: String;
    private expiryTime?: Number

    public getUrl(): String {
        return this.url;
    }

    public setUrl(url: String): void {
            url = url.endsWith("/") ? url.substring(0, url.length - 1) : url
            this.url = url;
    }

    public getAccessKey(): String {
        return this.accessKey;
    }

    public setAccessKey(accessKey: String): void {
        this.accessKey = accessKey;
    }

    public getSecretKey(): String {
        return this.secretKey;
    }

    public setSecretKey(secretKey: String): void {
        this.secretKey = secretKey;
    }

    public getToken(): String {
        return this.token;
    }

    public setToken(token: String): void {
        this.token = token;
    }

    public getExpiryTime(): Number {
        return this.expiryTime;
    }

    public setExpiryTime(expiryTime: Number): void {
        this.expiryTime = expiryTime;
    }


    constructor()
    {

    }

    


}