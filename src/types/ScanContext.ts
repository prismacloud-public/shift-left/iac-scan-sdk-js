import { ScanContextBuilder } from "./builder/ScanContextBuilder";
import { FailureCriteria } from "./FailureCriteria";
import { PrismaCloudConfig } from "./PrismaCloudConfig";
import { TemplateConfig } from "./TemplateConfig";

export class ScanContext
{
    private startTime: Date;
    private assetName: String;
    private assetType: String;
    private tags?: Map<String,String>;
    private scanAttributes?:Map<String,String>;
    private prismaCloudConfig: PrismaCloudConfig;
    private failureCriteria: FailureCriteria;
    private templateConfig: {};
    private payload: any;

    public getStartTime(): Date {
        return this.startTime;
    }

    public getAssetName(): String {
        return this.assetName;
    }

    public getAssetType(): String {
        return this.assetType;
    }

    public getTags?(): Map<String,String> {
        return this.tags;
    }

    public getScanAttributes?(): Map<String,String> {
        return this.scanAttributes;
    }

    public getPrismaCloudConfig(): PrismaCloudConfig {
        return this.prismaCloudConfig;
    }


    public getFailureCriteria(): FailureCriteria {
        return this.failureCriteria;
    }

    public getTemplateConfig(): {} {
        return this.templateConfig;
    }

    public getPayload(): any {
        return this.payload;
    }



    constructor(scanContextBuilder: ScanContextBuilder)
    {
        this.startTime = scanContextBuilder.startTime;
        this.assetName = scanContextBuilder.assetName;
        this.assetType = scanContextBuilder.assetType;
        this.tags = scanContextBuilder.tags;
        this.scanAttributes = scanContextBuilder.scanAttributes;
        this.prismaCloudConfig = scanContextBuilder.prismaCloudConfig;
        this.failureCriteria = scanContextBuilder.failureCriteria;
        this.templateConfig = scanContextBuilder.templateConfig;
        this.payload = scanContextBuilder.payload;

    }

}