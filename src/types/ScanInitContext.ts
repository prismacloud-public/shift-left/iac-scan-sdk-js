export class ScanInitContext
{
    private userAgent? : String

    public getUserAgent(): String {
        return this.userAgent;
    }

    public setUserAgent(userAgent: String): void {
        this.userAgent = userAgent;
    }


    constructor()
    {

    }
}