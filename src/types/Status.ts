export enum Status
{
    PASSED = "PASSED",
    FAILED = "FAILED"

}

export function getStatus(value: String)
{
    for (let item in Status) {
        if(item.valueOf() === value.toLocaleUpperCase())
        {
            return item;
        }
    }
}