import {TemplateType} from "./TemplateType"
import {TemplateParameters} from "./TemplateParameters"
import { TemplateConfigBuilder } from "./builder/TemplateConfigBuilder";

export class TemplateConfig
{
   private templateType: TemplateType;
   private templateVersion?: String;
   private templateParameters?: TemplateParameters


   constructor(templateConfigBuilder: TemplateConfigBuilder)
   {
      this.templateType =templateConfigBuilder.templateType;
      this.templateVersion = templateConfigBuilder.templateVersion;
      this.templateParameters = templateConfigBuilder.templateParameters;
   }
    public getTemplateType(): TemplateType {
        return this.templateType;
    }


    public getTemplateVersion?(): String {
        return this.templateVersion;
    }


    public getTemplateParameters?(): TemplateParameters {
        return this.templateParameters;
    }



}