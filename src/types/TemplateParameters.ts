import { TemplateParametersBuilder } from "./builder/TemplateParametersBuilder"

export class TemplateParameters{
    
    private variables?: {
      [k: string]: string
    }
    
    private variableFiles?: string[]
    
    private policyIdFilters?: string[]
    
    private files?: string[]
   
    private folders?: string[]

    constructor(templateParameterBuilder : TemplateParametersBuilder)
    {
        this.variables = templateParameterBuilder.variables;
        this.variableFiles = templateParameterBuilder.variableFiles;
        this.policyIdFilters = templateParameterBuilder.files;
        this.files = templateParameterBuilder.folders;
    }
}