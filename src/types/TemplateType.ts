export enum TemplateType
{
    CFT = "CFT",
    K8S = "K8S",
    TF = "TF"
}

export function getTemplateType(value: String)
{
    for (let item in TemplateType) {
        if(item.valueOf() === value.toLocaleUpperCase())
        {
            return item;
        }
    }
}