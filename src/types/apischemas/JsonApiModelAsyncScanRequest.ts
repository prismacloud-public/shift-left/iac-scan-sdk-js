export interface JsonApiModelAsyncScanRequest
{
    data: {
        /**
         * The required value must currently be async-scan
         */
        type?: string
        attributes: {
          /**
           * Registered asset name (255 character limit) that will appear as the resource name in the Prisma Cloud Devops Inventory
           */
          assetName: string
          /**
           * Supported asset types:
           * * AzureDevOps: Azure DevOps Services
           * * AWSCodePipeline: AWS CodePipeline
           * * BitbucketServer: Bitbucket server
           * * BitbucketCloud: Bitbucket Cloud
           * * CircleCI: CircleCI project
           * * GitHub: GitHub repo
           * * GitLab-CICD: GitLab CI/CD Pipeline
           * * GitLab-SCM: GitLab SCM Webhook
           * * IaC-API: direct IAC API attachment
           * * IntelliJ: IntelliJ IDE plugin managed files
           * * Jenkins: Jenkins build server
           * * VSCode: VSCode plugin managed files
           * * twistcli: Twistlock CLI attachment
           *
           */
          assetType: string
          /**
           * Tags assigned to the asset or the scan job. Both tag key and value have 255 character limit.
           */
          tags?: {
            [k: string]: string
          }
          /**
           * Additonal attributes associated with the IaC scan job. The keys below should be used for better display:
           * * buildnumber: For CI/CD, the build number associated with the IaC scan
           * * projectName: For CI/CD, the repo or project name associated with the IaC scan
           * * prName: For CI/CD, the pull/merge request name associated with the IaC scan
           * * pipelineName: For CD pipeline, the pipeline name associated with the IaC scan
           * * pipelineLambda: For CD pipeline, the lambda name associated with the IaC scan
           * * pipelineJobId: For CD pipeline, the job ID associated with the IaC scan
           * * pipelineStageName: For CD pipeline, the stage name associated with the IaC scan
           * * pipelineActionName: For CD pipeline, the action name associated with the IaC scan
           *
           */
          scanAttributes?: {
            [k: string]: string
          }
          /**
           * Thresholds that define scan asset result failures after evaluation.
           */
          failureCriteria?: {
            /**
             * Threshold for the number of high severity violations that define an asset failure.
             */
            high: number
            /**
             * Threshold for the number of medium severity violations that define an asset failure.
             */
            medium: number
            /**
             * Threshold for the number of low severity violations that define an asset failure.
             */
            low: number
            /**
             * Logic operator on failures [and, or]
             */
            operator: string
            
          }
          
        }
        
      }
      
}