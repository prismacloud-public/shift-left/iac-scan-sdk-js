export class ScanInitApiResponse
{
    /**
     * Example
     * {
  "data": {
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "links": {
      "url": "https://s3.amazonaws.com/s3sign2-bucket-hchq3nwuo8ns/s3-sign-demo.json?X-Amz-Security-Token=FQ..."
    }
  }
}
     */
    private data : ScanInitData

    public getData(): ScanInitData {
        return this.data;
    }

    public setData(data: ScanInitData): void {
        this.data = data;
    }

}

class ScanInitData
{
    private id : String
    private links : Links;

    public getId(): String {
        return this.id;
    }

    public setId(id: String): void {
        this.id = id;
    }

    public getLinks(): Links {
        return this.links;
    }

    public setLinks(links: Links): void {
        this.links = links;
    }

}

class Links{
    private url : String

    public getUrl(): String {
        return this.url;
    }

    public setUrl(url: String): void {
        this.url = url;
    }


}