import { FailureCriteria } from "../FailureCriteria";

export class FailureCriteriaBuilder

{

     high: Number
        
     medium: Number
    
     low: Number
    
     operator: String

    constructor()
    {
    }

    withHigh(high: Number)
    {
        this.high = high;
        return this;
    }

    withMedium(medium:Number)
    {
        this.medium = medium;
        return this;

    }

    withLow(low: Number)
    {
      this.low = low;
      return this;
    }

    withOperator(operator: String)
    {
        this.operator = operator;
        return this;
    }

    build()
    {
        return new FailureCriteria(this);
    }

}