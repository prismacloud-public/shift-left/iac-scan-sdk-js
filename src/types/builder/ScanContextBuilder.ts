import { FailureCriteria, TemplateConfig } from "..";
import { PrismaCloudConfig } from "../PrismaCloudConfig";
import { ScanContext } from "../ScanContext";

export class ScanContextBuilder
{
     startTime: Date
     assetName: String;
     assetType: String;
     tags?: Map<String,String>;
     scanAttributes?:Map<String,String>;
     prismaCloudConfig: PrismaCloudConfig;
     failureCriteria: FailureCriteria;
     templateConfig: {};
     payload: any

    public withStartTime(startTime: Date)
    {
        this.startTime = startTime;
        return this;
    }
    public withAssetName(assetName: String) {
        this.assetName = assetName;
        return this;
    }

    

    public withAssetType(assetType: String) {
        this.assetType = assetType;
        return this;
    }

    
    public withTags?(tags: Map<String,String>){
        this.tags = tags;
        return this;
    }

   

    public withScanAttributes?(scanAttributes: Map<String,String>) {
        this.scanAttributes = scanAttributes;
        return this;
    }

    
    public withPrismaCloudConfig(prismaCloudConfig: PrismaCloudConfig) {
        this.prismaCloudConfig = prismaCloudConfig;
        return this;
    }

    

    public withFailureCriteria(failureCriteria: FailureCriteria) {
        this.failureCriteria = failureCriteria;
        return this;
    }

    

    public withTemplateConfig(templateConfig: {}) {
        this.templateConfig = templateConfig;
        return this;
    }

    public withPayload(payload: any)
    {
        this.payload = payload;
        return this;
    }
    constructor()
    {

    }
    build()
    {
        return new ScanContext(this);
    }
}