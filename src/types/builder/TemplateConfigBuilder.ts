import { TemplateConfig } from "../TemplateConfig";
import { TemplateParameters } from "../TemplateParameters";
import { TemplateType } from "../TemplateType";

export class TemplateConfigBuilder{

     templateType: TemplateType;
    templateVersion?: String;
    templateParameters?: TemplateParameters

    withTemplateType(templateType: TemplateType)
    {
        this.templateType = templateType;
        return this;
    }

    withTemplateVersion(templateVersion: String)
    {
        this.templateVersion = templateVersion;
        return this;
    }

    withTemplateParameters(templateParameters: TemplateParameters)
    {
        this.templateParameters = templateParameters;
        return this;
    }

    build(){
        return new TemplateConfig(this);
    }



}