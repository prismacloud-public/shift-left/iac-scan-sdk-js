import { TemplateParameters } from "../TemplateParameters"


export class TemplateParametersBuilder{

     variables?: {
        [k: string]: string
      }
     
       variableFiles?: string[]
      
       policyIdFilters?: string[]
      
       files?: string[]
      
       folders?: string[]

      constructor()
      {}

      withVariables(variables?: {
        [k: string]: string
      }){
        this.variables = variables;
        return this;

      }

      withVariableFiles(variableFiles?: string[]) {
          this.variableFiles = variableFiles;
          return this;
      }

      withPolicyIdFilters(policyIdFilters?: string[]){
          this.policyIdFilters = policyIdFilters;
          return this;
      }

      withFiles(files?: string[]){
          this.files = files;
          return this;
      }

      withFolders(folders?: string[])
      {
          this.folders = folders;
          return this;
      }

      build()
      {
          return new TemplateParameters(this);
        
      }

}