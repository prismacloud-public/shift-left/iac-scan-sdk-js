import * as path from 'path';
import * as fs from "fs";
import * as os from "os";

const JSZip = require('jszip-sync');
import * as ziputil from '../../src/util/ZipUtil'

const fg = require('fast-glob');
const defaultPatternsToBeIncluded = ['**/*.json','**/*.yaml','**/*.yml','**/*.tf','**/*.tfvars','**/*.sh','**/tf/*']
const defaultPatternsToBeExcluded = ['**/node_modules/**','**/.idea/**','**/.vscode/**','**/.git/**','**/.DS_Store/**','**/.terraform/**','**/*.zip','**/*.tar','**/*.jar','**/*.gzip','**/*.rar']
const terraformPatterns = ['**/*.tf','**/tf.json','**/.tfvars','**/tfvars.json']
export async function getTerraformFolders(directoryPath: string) : Promise<string[]> {


    let terraform = await fg(terraformPatterns, { dot: true, objectMode: true,cwd: directoryPath });
    let paths : string[] = terraform.map(item =>{return item.path.replace(item.name,'').concat("**")})
    return paths;

}

export async function  filterfiles(directoryPath : string, includePattern? :string[]|undefined, excludePattern? : string[]|undefined) {

    let include = [].concat(defaultPatternsToBeIncluded);
    let terraformFolders = await getTerraformFolders(directoryPath)
    if(terraformFolders != undefined && terraformFolders.length > 0)
    {
        include = include.concat(terraformFolders);
    }
    if(includePattern != undefined && includePattern.length > 0)
    {
        include =  include.concat(includePattern);
    }

    let exclude = [].concat(defaultPatternsToBeExcluded);
    if(excludePattern != undefined && excludePattern.length > 0)
    {
        exclude =  exclude.concat(excludePattern);
    }


    let enteries;
    if(directoryPath == undefined || directoryPath == null || directoryPath == '' )
    {
        enteries = await fg(include, { dot: true, ignore : exclude});
    }
    else
    {
        enteries = await fg(include, { dot: true, ignore : exclude,cwd: directoryPath});

    }

    return enteries;
}

export async function zipRepo(directoryPath, repoPath) {
    let zipFile = await ziputil.getZippedFolderArchive(directoryPath, repoPath);
    var content: any = await ziputil.readZipFile(repoPath);
    console.log("Content length: " + content.length);
    return content;
}

export async  function zipFiles(directoryPath, repoPath, allPaths)
{
    let zip = new JSZip()
    let zipped = zip.sync(() => {
        for (let filePath of allPaths) {

            //let addPath = path.relative(path.join(dir, ".."), filePath)
            let addPath = filePath// use this instead if you don't want the source folder itself in the zip

            let fileData = fs.readFileSync(path.resolve(directoryPath,filePath))
            if(os.type() == "Windows_NT") {
                const str = '\\\\';
                const replaceStr = new RegExp(str, 'g');
                addPath = addPath.replace(replaceStr,'/');
            }
            zip.file(addPath, fileData)
        }
        let fileData = null;
        zip.generateAsync({type: "nodebuffer"}).then((content:any) => {
            fileData = content;
        });
        return fileData;
    })
    return zipped;
}

export async function filterAndZip(directoryPath : string, repoPath: String, iacAttributes : any)
{
    let includePatterns : string[] | undefined = undefined;
    let excludePatterns : string[] | undefined = undefined;
    if(iacAttributes != undefined)
    {
        if(iacAttributes['includePattern'] != undefined && Array.isArray(iacAttributes['includePattern']) && iacAttributes['includePattern'].length > 0)
        {
            includePatterns = iacAttributes['includePattern'];
        }

        if(iacAttributes['excludePattern'] != undefined && Array.isArray(iacAttributes['excludePattern']) && iacAttributes['excludePattern'].length > 0)
        {
            excludePatterns = iacAttributes['excludePattern']
        }
    }
    let allPaths = await filterfiles(directoryPath,includePatterns, excludePatterns)
    return await zipRepo(directoryPath, repoPath);
}