export const SUPPORTED_TEMPLATES = ["CFT","K8S","TF"]

    /**
     * Null and empty check is left to the clients as not all the clients expect the value to be present in config.yml
     */
    export function validateTemplateType(attributes:{}): {status : boolean, message?:string}
    {
        let isValid : boolean = false;
        if (typeof attributes['templateType'] !== 'undefined' &&  attributes['templateType'] != '' &&
            SUPPORTED_TEMPLATES.indexOf(String(attributes['templateType']).toUpperCase()) >=0)
        {
            isValid = true
        }
        let summary;
        if(!isValid) {
            summary = 'No valid template_type found. Please make sure you have entered either of these values: ' + SUPPORTED_TEMPLATES
            console.error(summary)

        }
        return {status : isValid, message :summary}
    }
