import {Error} from "../types";

export function convertJsonErrorsToMarkdownErrors(error:Error, fileName?)
{
    let errorsInMd = ''
    const replaceRegex = new RegExp(fileName != 'undefined' ? fileName + '/' : '', 'g')

    errorsInMd += '### Errors\n\nError Status | Error Message\n------------ | -------------\n'
    if(error.getServerMessage() != undefined && error.getServerMessage().errors != undefined && Array.isArray(error.getServerMessage().errors))
    {
        let serverError = error.getServerMessage().errors;
        for (let i = 0; i < serverError.length; i++) {
            const failure = serverError[i]
            const message = failure.detail.replace(replaceRegex, '').replace(/\r?\n|\r/g, ' <br /> ')
            errorsInMd += failure.status + ' | ' + message + '\n'

        }
    }
    else
    {
        const message = error.getMessage().replace(replaceRegex, '').replace(/\r?\n|\r/g, ' <br /> ')
        errorsInMd += error.getName() + ' | ' + message + '\n'
    }
    return errorsInMd;
}


export function removeRootPathFromMarkdown(markdown:string, fileName?)
{
    const replaceRegex = new RegExp(fileName != 'undefined' ? fileName + '/' : '', 'g')



    return markdown.replace(replaceRegex, '')


}