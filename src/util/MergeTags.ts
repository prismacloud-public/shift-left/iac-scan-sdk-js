/**
 *
 * @param args can have any set of tags from 1 to n, now either it will be an array of strings something like
 * ['key1:value1','key2:value2'] or an object {'key1':'value1_1', 'key3':'value3'} or a comma separated string 'key3:value3_3,key4:value4' , the final result will be
 * {
 *     'key1' : 'value1_1'
 *     'key2' : 'value2',
 *     'key3' :'value3_3'
 *     'key4' : 'value4'
 * }
 *
 * If there are same keys  than the later one in the arguments list will override the previous one.
 */

export function mergeTags(...args) :{}
{
    let mergedTags ={}
    for(var i=0; i<args.length; i++)
    {
        if(!(args[i] == undefined || args[i] == '')) {
            if (typeof args[i] === 'string' || args[i] instanceof String) {
                let commaSeparated = args[i].split(',');
                for (var k = 0; k < commaSeparated.length; k++) {
                    let keyValue = commaSeparated[k].split(':');
                    mergedTags[keyValue[0].trim()] = keyValue.slice(1).join(':');
                }
            } else if (Array.isArray(args[i])) {
                for (var j = 0; j < args[i].length; j++) {
                    let keyValue = args[i][j].split(':');
                    mergedTags[keyValue[0].trim()] = keyValue.slice(1).join(':');
                }

            } else {
                for (var key in args[i]) {
                    if (args[i].hasOwnProperty(key)) {
                        mergedTags[key] = args[i][key]
                    }
                }
            }

        }

    }
    return mergedTags;
}