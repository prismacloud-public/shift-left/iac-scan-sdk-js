import {readTemplateConfigFromRepo} from "../service";
import {validateTemplateType} from "./IacConfigValidator";

export async function readConfigFileAndValidate(yamAsString:string)
{
    let templateAttributes;
    try {
        templateAttributes = await readTemplateConfigFromRepo(yamAsString);
    } catch (e) {
        let summary = "Error Reading config.yml under .prismaCloud folder in repo's root folder. Please make sure the file is present and is in correct format (sample file - https://docs.paloaltonetworks.com/prisma/prisma-cloud/prisma-cloud-admin/prisma-cloud-devops-security/set-up-your-prisma-cloud-configuration-file-for-iac-scan.html)"
        throw new Error(summary)
    }
    let result = validateTemplateType(templateAttributes);
    if (!result['status']) {
        throw new Error(result['message'])
    }
    return templateAttributes;
}