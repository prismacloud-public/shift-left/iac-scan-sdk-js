
export async function getVersion()
{
    let pjson = null;
    try {
        pjson = require('./../../package.json');
    }
    catch (e)
    {
        // when packaging this goes in dist folder hence checking in one folder above.
        pjson = require('./../../../package.json');
    }
    if(pjson != null) {
        console.log("prisma scan sdk version being used is :" + pjson.version);
        return pjson.version;
    }
}

getVersion()