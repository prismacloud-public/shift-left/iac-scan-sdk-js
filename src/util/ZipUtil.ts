import * as fs from 'fs'
import * as path from 'path'
const JSZip = require("jszip-sync");

export async function readZipFile(filePath: string) {
	//var fileBuffer = Buffer.from(filePath, 'base64');
	//console.log("filebuffer: " + fileBuffer.length);
	//return fileBuffer;
  
	return new Promise((resolve, reject) => {
		fs.readFile(filePath, function(err, data) {
		  if (err) {
			  reject(err);
		  } else {
			//console.log("data: " + data.length);
			  resolve(data);
		  }
	  });
	})
  }
  
  export function readFile(filePath: string): any{
	console.log("file path: " + filePath)
	var file_reader = fs.createReadStream(filePath, {encoding: 'binary'});
	var file_contents = '';
	file_reader.on('data', function(data)
	{
	  file_contents += data;  
	});
	console.log("file content: " + file_contents.length);
	return file_contents;
  }
  
  export async function getZippedFolderArchive(dir: any, repoPath: any) {
	var archiver = require('archiver');
	  return new Promise((resolve, reject) => {
		var archive = archiver('zip', {
		  zlib: { level: 9 }
		})
	   const output = fs.createWriteStream(repoPath)
	   output.on('close', function() {
		console.log('zipDirectory finished: ' + archive.pointer() + ' total bytes')
		resolve(true)
	   })
	   output.on('end', function() {
		 resolve(true)
	   })
	 
	   archive.on('error', function() {
		 reject(false)
		})
  
	   archive.on('warning', function() {
		 console.log("Warning")
	   })
  
	   archive.pipe(output)
	 
	   archive.glob('**/*',
	   {
		cwd: dir,
		ignore: ['*.zip']
	   });
	   archive.finalize()
	  })
  }
  
  
