import {expect} from 'chai'
import * as auth from '../../src/service/AuthenticationService'
import * as nock from 'nock'
import { PluginType, PrismaCloudConfig } from '../../src/types'
const url = 'https://api.test.com'
const username = 'user'
const password = 'pass'
const token = 'eyJhbGciOiJIUzI1NiJ9.eyJmaXJzdExvZ2luIjpmYWxzZSwiaXNzIjoiaHR0cHM6Ly9hcGlrOHN0YWdlLms4cy5wcmlzbWFjbG91ZC5pbyIsInJlc3RyaWN0IjowLCJpc0FjY2Vzc0tleUxvZ2luIjp0cnVlLCJ1c2VyUm9sZVR5cGVOYW1lIjoiU3lzdGVtIEFkbWluIiwiaXNTU09TZXNzaW9uIjpmYWxzZSwibGFzdExvZ2luVGltZSI6MTYwNzYyMzcwMTY1MywidXNlclJvbGVUeXBlSWQiOjEsInNlbGVjdGVkQ3VzdG9tZXJOYW1lIjoiUEFOVy1kZXYiLCJzZXNzaW9uVGltZW91dCI6MzAwLCJ1c2VyUm9sZUlkIjoiMGY4MDgzZDktNGE0OS00OTNlLWExZTUtMTFmNTU3N2Q3MGE3IiwiaGFzRGVmZW5kZXJQZXJtaXNzaW9ucyI6dHJ1ZSwiZXhwIjoxNjA3OTM0NjY1LCJ1c2VybmFtZSI6Im5pYmFuc2FsQHBhbG9hbHRvbmV0d29ya3MuY29tIiwidXNlclJvbGVOYW1lIjoiU3lzdGVtIEFkbWluIn0.PTk_QIJU3b0RZfQzpR4lsJ_76hqiL7VxosqSUUdaiJE'
const successFullTokenResult = {token : token}
describe("get token from authenticate", () => {
	let authenticationService : auth.AuthenticationService;
	let prismaCloudConfig : PrismaCloudConfig;
	beforeEach(function() {
    authenticationService = new auth.AuthenticationService();
	prismaCloudConfig = new PrismaCloudConfig();
	prismaCloudConfig.setUrl(url);
	prismaCloudConfig.setAccessKey(username);
	prismaCloudConfig.setSecretKey(password)
    });
    
	it('gets jwt token successfully', () => {
		nock(url)
  		.post('/login', { username: 'user', password: 'pass' })
		  .reply(200, successFullTokenResult)
		return authenticationService.getAuthenticationToken(prismaCloudConfig,PluginType.GITHUB_APP).then(result => {
			expect (result['token']).to.be.equal(token)
			expect(result['expiryTime']).to.be.greaterThan(0);
		})
})

it('returns bad request', function () {
	nock(url)
  		.post('/login', { username: 'user', password: 'pass' })
		  .reply(401, {
			"message": "any bad message"
		})
	return authenticationService.getAuthenticationToken(prismaCloudConfig, PluginType.GITHUB_APP).then(function (result) {}
	)
	.catch(function(m) { 
		expect(m.code).to.equal(401); 
		expect(m.message).to.be.string;
		expect(m.serverMessage).to.deep.equal({
			"message": "any bad message"
		})
	});
})

it('Not found', function () {
	nock(url)
  		.post('/login', { username: 'user', password: 'pass' })
		  .reply(404, {})
	return authenticationService.getAuthenticationToken(prismaCloudConfig, PluginType.GITHUB_APP).then(function (result) {}
	)
	.catch(function(m) { 
		expect(m.code).to.equal(404); 
		expect(m.message).to.be.string; 
	});
})

it('Internal server error', function () {
	nock(url)
  		.post('/login', { username: 'user', password: 'pass' })
		  .reply(500, {})
	return authenticationService.getAuthenticationToken(prismaCloudConfig, PluginType.GITHUB_APP).then(function (result) {}
	)
	.catch(function(m) { 
		expect(m.code).to.equal(500); 
		expect(m.message).to.be.string; 
	});
})

it('Token not expired', function () {
	prismaCloudConfig.setToken(token);
	prismaCloudConfig.setExpiryTime(Math.round(((new Date()).getTime() / 1000) + 5*60 ));
	return authenticationService.getAuthenticationToken(prismaCloudConfig, PluginType.GITHUB_APP)
	.then(function (result) {
		expect (result['token']).to.be.equal(token)
	}
	);
})

it('gets jwt token successfully when token expired', () => {
	prismaCloudConfig.setToken(token);
	prismaCloudConfig.setExpiryTime(Math.round(((new Date()).getTime() / 1000) - 5*60 ));
	nock(url)
	  .post('/login', { username: 'user', password: 'pass' })
	  .reply(200, successFullTokenResult)
	return authenticationService.getAuthenticationToken(prismaCloudConfig,PluginType.GITHUB_APP).then(result => {
		expect (result['token']).to.be.equal(token)
		expect(result['expiryTime']).to.be.greaterThan(0);
	})
})

	})
