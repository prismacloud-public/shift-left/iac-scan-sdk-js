import {expect} from 'chai'
import * as ss from '../../src/service/ScanStatus'
import * as sss from '../../src/service/ScanStatusService'

import * as nock from 'nock'
import { PluginType, PrismaCloudConfig } from '../../src/types'
import { awaitScanCompletion } from '../../src/service/AwaitScanCompletion'
const url = 'https://api.test.com'
const username = 'user'
const password = 'pass'
const token = 'eyJhbGciOiJIUzI1NiJ9.eyJmaXJzdExvZ2luIjpmYWxzZSwiaXNzIjoiaHR0cHM6Ly9hcGlrOHN0YWdlLms4cy5wcmlzbWFjbG91ZC5pbyIsInJlc3RyaWN0IjowLCJpc0FjY2Vzc0tleUxvZ2luIjp0cnVlLCJ1c2VyUm9sZVR5cGVOYW1lIjoiU3lzdGVtIEFkbWluIiwiaXNTU09TZXNzaW9uIjpmYWxzZSwibGFzdExvZ2luVGltZSI6MTYwNzYyMzcwMTY1MywidXNlclJvbGVUeXBlSWQiOjEsInNlbGVjdGVkQ3VzdG9tZXJOYW1lIjoiUEFOVy1kZXYiLCJzZXNzaW9uVGltZW91dCI6MzAwLCJ1c2VyUm9sZUlkIjoiMGY4MDgzZDktNGE0OS00OTNlLWExZTUtMTFmNTU3N2Q3MGE3IiwiaGFzRGVmZW5kZXJQZXJtaXNzaW9ucyI6dHJ1ZSwiZXhwIjoxNjA3OTM0NjY1LCJ1c2VybmFtZSI6Im5pYmFuc2FsQHBhbG9hbHRvbmV0d29ya3MuY29tIiwidXNlclJvbGVOYW1lIjoiU3lzdGVtIEFkbWluIn0.PTk_QIJU3b0RZfQzpR4lsJ_76hqiL7VxosqSUUdaiJE'
const attributes = {
    id :'3fa85f64-5717-4562-b3fc-2c963f66afa6'
}
const successResult = {
    "data": {
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "attributes": {
        "status": "passed"
      }
    }
  }
 const error = {
    errors: [
      {
        status: "string",
        code: "405",
        detail: "Invalid template file provided",
        source: "Scan-worker-service"
      }
    ]
  }   
describe("tests for getting scan status ", () => {
    let prismaCloudConfig : PrismaCloudConfig;
    let startTime = new Date(Math.round(((new Date()).getTime() ) - 5*60 *1000 ));
    let timeInSeconds = Math.round(((new Date()).getTime() / 1000) + 10*60 );
	beforeEach(function() {
	prismaCloudConfig = new PrismaCloudConfig();
	prismaCloudConfig.setUrl(url);
	prismaCloudConfig.setAccessKey(username);
    prismaCloudConfig.setSecretKey(password);
    prismaCloudConfig.setToken(token);
    
    prismaCloudConfig.setExpiryTime(timeInSeconds);
    });

    it('awaits for scan completion', () => {
		nock(url)
  		.get('/iac/v2/scans/'+ attributes.id +'/status')
          .reply(200, successResult)
          return awaitScanCompletion(startTime,attributes, prismaCloudConfig, PluginType.GITHUB_APP,{'User-Agent':'Github App/2.0.0'},1)
          .then(result=>{
              expect(result).to.deep.equal(successResult)
          })
        })

        it('awaits for scan completion returns 500', () => {
            nock(url)
              .get('/iac/v2/scans/'+ attributes.id +'/status')
              .reply(500, successResult)
              return awaitScanCompletion(startTime,attributes, prismaCloudConfig, PluginType.GITHUB_APP,{'User-Agent':'Github App/2.0.0'},1)
              .catch(err=>{
                  expect(err.code).to.equal(500)
                  expect(err.name).to.deep.equal('INTERNAL_ERROR')
              })
            })

            it('throws timeout', () => {
                  return awaitScanCompletion(new Date(Math.round(((new Date()).getTime() ) - 15*60 *1000 )),attributes, prismaCloudConfig, PluginType.GITHUB_APP,{'User-Agent':'Github App/2.0.0'},1)
                  .catch(err=>{
                      expect(err.message).to.contains(attributes['id'])
                      expect(err.name).to.deep.equal('TIME_OUT')
                  })
                })    
})