import {expect} from 'chai'
import * as sr from '../../src/service/GetScanResults'
import * as rss from '../../src/service/GetScanResultsService'

import * as nock from 'nock'
import { PluginType, PrismaCloudConfig } from '../../src/types'
const url = 'https://api.test.com'
const username = 'user'
const password = 'pass'
const token = 'eyJhbGciOiJIUzI1NiJ9.eyJmaXJzdExvZ2luIjpmYWxzZSwiaXNzIjoiaHR0cHM6Ly9hcGlrOHN0YWdlLms4cy5wcmlzbWFjbG91ZC5pbyIsInJlc3RyaWN0IjowLCJpc0FjY2Vzc0tleUxvZ2luIjp0cnVlLCJ1c2VyUm9sZVR5cGVOYW1lIjoiU3lzdGVtIEFkbWluIiwiaXNTU09TZXNzaW9uIjpmYWxzZSwibGFzdExvZ2luVGltZSI6MTYwNzYyMzcwMTY1MywidXNlclJvbGVUeXBlSWQiOjEsInNlbGVjdGVkQ3VzdG9tZXJOYW1lIjoiUEFOVy1kZXYiLCJzZXNzaW9uVGltZW91dCI6MzAwLCJ1c2VyUm9sZUlkIjoiMGY4MDgzZDktNGE0OS00OTNlLWExZTUtMTFmNTU3N2Q3MGE3IiwiaGFzRGVmZW5kZXJQZXJtaXNzaW9ucyI6dHJ1ZSwiZXhwIjoxNjA3OTM0NjY1LCJ1c2VybmFtZSI6Im5pYmFuc2FsQHBhbG9hbHRvbmV0d29ya3MuY29tIiwidXNlclJvbGVOYW1lIjoiU3lzdGVtIEFkbWluIn0.PTk_QIJU3b0RZfQzpR4lsJ_76hqiL7VxosqSUUdaiJE'
const attributes = {
    id :'3fa85f64-5717-4562-b3fc-2c963f66afa6'
}
const successResult = {
    "meta": {
      "matchedPoliciesSummary": {
        "high": 6,
        "medium": 15,
        "low": 20
      },
      "errorDetails": [
        {
          "status": "string",
          "code": "405",
          "detail": "Invalid template file provided",
          "source": "Scan-worker-service"
        }
      ]
    },
    "data": [
      {
        "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "attributes": {
          "severity": "high",
          "name": "AWS Security Groups allow internet traffic to SSH port (22)",
          "rule": "$.resource[*].aws_security_group exists and ($.resource.aws_security_group[*].ingress.cidr_blocks contains 0.0.0.0/0",
          "desc": "This policy identifies AWS Security Groups which do allow inbound traffic on SSH port (22) from public internet",
          "systemDefault": true,
          "files": [
            "./main.tf"
          ],
          "policyId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          "docUrl": "https://docs.paloaltonetworks.com/prisma/prisma-cloud/prisma-cloud-policy-reference/configuration-policies/configuration-policies-build-phase/amazon-web-services-configuration-policies/policy_617b9138-584b-4e8e-ad15-7fbabafbed1a.html"
        }
      }
    ]
  }
 const error = {
    errors: [
      {
        status: "string",
        code: "405",
        detail: "Invalid template file provided",
        source: "Scan-worker-service"
      }
    ]
  }   
describe("tests for getting scan results ", () => {
	let scanResults : sr.GetScanResults;
	let prismaCloudConfig : PrismaCloudConfig;
	beforeEach(function() {
    scanResults = new rss.GetScanResultsService();
	prismaCloudConfig = new PrismaCloudConfig();
	prismaCloudConfig.setUrl(url);
	prismaCloudConfig.setAccessKey(username);
    prismaCloudConfig.setSecretKey(password);
    prismaCloudConfig.setToken(token);
    prismaCloudConfig.setExpiryTime(Math.round(((new Date()).getTime() / 1000) + 5*60 ));
    });

    it('starts scan init successfully', () => {
		nock(url)
  		.get('/iac/v2/scans/'+ attributes.id +'/results')
          .reply(200, successResult)
          
		return scanResults.getScanResults(attributes, prismaCloudConfig, PluginType.GITHUB_APP,{'User-Agent':'Github App/2.0.0'}).then(result => {
			expect (result).to.deep.equal(successResult)
		})
})

        it('Scan init throws 401', () => {
            nock(url)
            .get('/iac/v2/scans/'+ attributes.id+'/status')
            .reply(401, error)
            
            scanResults.getScanResults(attributes, prismaCloudConfig, PluginType.GITHUB_APP,{'User-Agent':'Github App/2.0.0'})
            .catch(function(m) { 
                expect(m.code).to.equal(401)
            })
            
        })
})