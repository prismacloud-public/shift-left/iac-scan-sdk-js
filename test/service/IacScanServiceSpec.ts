import {expect} from 'chai'
import * as iacscan from '../../src/service/IacScanService'
import * as nock from 'nock'
import { PluginType, PrismaCloudConfig } from '../../src/types'
import path = require('path')
import * as ziputil from '../../src/util/ZipUtil'
import * as jwt from 'jsonwebtoken'
const url = 'https://api.test.com'
const username = 'user'
const password = 'pass'
const token = jwt.sign({'username' : 'user', 'password':'pass'},'secretkey',{expiresIn : '10m'})
const successFullTokenResult = {token : token}
const failureCriteria = {
    "high": 1,
    "medium": 10,
    "low": 30,
    "operator": "or"
}
const initScanRequest = {
    "data": {
      "type": "async-scan",
      "attributes": {
        "assetName": "my-asset",
        "assetType": "AzureDevOps",
        "tags": {
          "env": "dev",
          "region": "us-west-1"
        },
        "scanAttributes": {
          "projectName": "my-project",
          "prName": "SL-1234"
        },
        "failureCriteria": failureCriteria
      }
    }
  }

  const successScanInitResult = {
    "data": {
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "links": {
        "url": "https://s3.amazonaws.com/s3sign2-bucket-hchq3nwuo8ns/s3-sign-demo.json?X-Amz-Security-Token=FQ"
      }
    }
  }

  const triggerScanBody = {
    "data": {
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "attributes": {
        "templateType": "tf",
        "templateVersion": "0.13",
        "templateParameters": {
          "variables": {
            "region": "us-east-1",
            "image_id": "amzn2-ami-hvm-2.0"
          },
          "variableFiles": [
            "./dev.tfvars",
            "./us/qa.tfvars"
          ],
          "policyIdFilters": [
            "123e4567-e89b-12d3-a456-426614174000"
          ],
          "files": [
            "./dev/auto_scale.tf",
            "./dev/app.tf"
          ],
          "folders": [
            "./dev",
            "./under_develop",
            "./modules/dev"
          ]
        }
      }
    }
  }
  var successScanStatus = {
    "data": {
        "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "attributes": {
            "status": "passed"
        }
    }
};
const successResult =  {
    "meta": {
      "matchedPoliciesSummary": {
        "high": 6,
        "medium": 15,
        "low": 20
      },
      "errorDetails": [
        {
          "status": "string",
          "code": "405",
          "detail": "Invalid template file provided",
          "source": "Scan-worker-service"
        }
      ]
    },
    "data": [
      {
        "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "attributes": {
          "severity": "high",
          "name": "AWS Security Groups allow internet traffic to SSH port (22)",
          "rule": "$.resource[*].aws_security_group exists and ($.resource.aws_security_group[*].ingress.cidr_blocks contains 0.0.0.0/0",
          "desc": "This policy identifies AWS Security Groups which do allow inbound traffic on SSH port (22) from public internet",
          "systemDefault": true,
          "files": [
            "./main.tf"
          ],
          "policyId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          "docUrl": "https://docs.paloaltonetworks.com/prisma/prisma-cloud/prisma-cloud-policy-reference/configuration-policies/configuration-policies-build-phase/amazon-web-services-configuration-policies/policy_617b9138-584b-4e8e-ad15-7fbabafbed1a.html"
        }
      }
    ]
  }
const error = {
    errors: [
        {
            status: "string",
            code: "405",
            detail: "Invalid template file provided",
            source: "Scan-worker-service"
        }
    ]
}
describe("perform iac scan successfully", () => {
    let scanContext = {
        url : url,
        accessKey : username,
        secretKey : password,
        userAgent : 'Github App/2.0.0',
        pluginType : PluginType.GITHUB_APP,
        startTime : new Date(),
        iacAttributes : triggerScanBody.data.attributes,
        failureCriteria: failureCriteria,
        assetName: "my-asset",
        assetType: "AzureDevOps",
        tags : {"env": "dev", "region": "us-west-1"},
        scanAttributes : {"projectName": "my-project",
            "prName": "SL-1234"},
        payload : ziputil.getZippedFolderArchive(path.resolve(__dirname, "../zipfolder"), path.resolve(__dirname, "/repo.zip"))

    }
	let iacScanService : iacscan.IacScanService;
	beforeEach(function() {
        this.timeout(0)
    iacScanService = new iacscan.IacScanService();


    });
    
	it('get results successully', function() {
        this.timeout(0)
		nock(url)
  		.post('/login', { username: 'user', password: 'pass' })
        .reply(200, successFullTokenResult)
         
        .post('/iac/v2/scans', initScanRequest)
          .reply(200, successScanInitResult) 
        
        .post('/iac/v2/scans/'+ triggerScanBody.data.id, triggerScanBody)
        .reply(200, {}) 
        .get('/iac/v2/scans/'+ triggerScanBody.data.id +'/status')
          .reply(200, successScanStatus)
        .get('/iac/v2/scans/'+ triggerScanBody.data.id +'/results')
        .reply(200, successResult) 
          

        nock('https://s3.amazonaws.com')
          .put('/s3sign2-bucket-hchq3nwuo8ns/s3-sign-demo.json?X-Amz-Security-Token=FQ')
        .reply(200, {})

		return iacScanService.getScanResultsJsonModel(scanContext).then(result => {
			expect(result.scanResults).to.deep.equal(successResult)
		})
})

    it('Scan status throws 401', function() {
        this.timeout(0)
        nock(url)
            .post('/login', { username: 'user', password: 'pass' })
            .reply(200, successFullTokenResult)

            .post('/iac/v2/scans', initScanRequest)
            .reply(200, successScanInitResult)

            .post('/iac/v2/scans/'+ triggerScanBody.data.id, triggerScanBody)
            .reply(200, {})
            .get('/iac/v2/scans/'+ triggerScanBody.data.id+'/status')
            .reply(401, error)
            .get('/iac/v2/scans/'+ triggerScanBody.data.id +'/results')
            .reply(200, successResult)


        nock('https://s3.amazonaws.com')
            .put('/s3sign2-bucket-hchq3nwuo8ns/s3-sign-demo.json?X-Amz-Security-Token=FQ')
            .reply(200, {})




        return iacScanService.getScanResultsJsonModel(scanContext).catch(result => {
            expect(result.code).to.be.equal(401)
            expect(result.serverMessage).to.deep.equal(error)
        })
    })


    it('get results successully in MD format', function() {
        this.timeout(0)

        const markDownResult : string = "### Prisma Cloud IaC Scan - Failed\n" +
            "\n" +
            "Prisma Cloud IaC scan has found **3 issues** with Severity High: 1, Medium: 1 , Low:  1\n" +
            "Scan **FAILED** as per the configured failure criteria High: 1, Medium: 1, Low: 1, Operator: OR\n" +
            "\n" +
            "| Severity |                                 Policy Name                                 |                                         Files                                         |\n" +
            "|:--------:|:---------------------------------------------------------------------------:|:-------------------------------------------------------------------------------------:|\n" +
            "|   high   |      [AWS S3 buckets are accessible to public](https://prisma.fyi/p71)      | ./cft_test/cft_aws_json_all_issues.json<br />./cft_test/cft_aws_json_with_issues.json |\n" +
            "|  medium  |       [AWS S3 Object Versioning is disabled](https://prisma.fyi/p69)        | ./cft_test/cft_aws_json_all_issues.json<br />./cft_test/cft_aws_json_with_issues.json |\n" +
            "|   low    | [AWS S3 buckets do not have server side encryption](https://prisma.fyi/p95) | ./cft_test/cft_aws_json_all_issues.json<br />./cft_test/cft_aws_json_with_issues.json |"
        const markDownResponse = {
            "meta": {
                "matchedPoliciesSummary": {
                    "high": 1,
                    "medium": 1,
                    "low": 1
                },
                "status": "failed"
            },
            "data": {
                "id" : "abcdef",
                "attributes" : {
                    "title" : "scan failed",
                    "result" : "md"
                }
            }
        }

        nock(url)
            .post('/login', { username: 'user', password: 'pass' })
            .reply(200, successFullTokenResult)

            .post('/iac/v2/scans', initScanRequest)
            .reply(200, successScanInitResult)

            .post('/iac/v2/scans/'+ triggerScanBody.data.id, triggerScanBody)
            .reply(200, {})
            .get('/iac/v2/scans/'+ triggerScanBody.data.id +'/status')
            .reply(200, successScanStatus)
            .get('/iac/v2/scans/'+ triggerScanBody.data.id +'/results/markdown')
            .reply(200, markDownResponse)


        nock('https://s3.amazonaws.com')
            .put('/s3sign2-bucket-hchq3nwuo8ns/s3-sign-demo.json?X-Amz-Security-Token=FQ')
            .reply(200, {})


        return iacScanService.getScanResultsInMarkdownFormat(scanContext).then(result => {
            expect(result).to.deep.equal(markDownResponse)
        })
    })
})