import {expect} from 'chai'
import * as fs from 'fs'
import * as path from 'path'
import * as templatereader from '../../src/service/RepoTemplateConfigYamlReader'

describe("read readTemplateConfigFromRepo()", () => {

	it('resolves with t012 config file', () => {
		return templatereader.readTemplateConfigFromRepo(fs.readFileSync(path.resolve(__dirname, "../config-files/t012.yml"),'utf-8')).then(result => {
			expect(result).to.deep.equal({
				templateType:'TF',
				templateVersion: 0.12,
				templateParameters:{variableFiles:['vf1.tf','vf2.tf','vf3.tf','scan/rich-value-types/'],
				variables:{check1:'public-read-write1',varName1:'varValue1', check2:'public-read-write2',varName2:'varValue2'}},
				tags:{abc:'xyz:rtu',pqw:'oiu'}});
			})
		})

	it('resolves with t012 config file with root_module', () => {
		return templatereader.readTemplateConfigFromRepo(fs.readFileSync(path.resolve(__dirname, "../config-files/tf012withRootModules.yml"),'utf-8')).then(result => {
			expect(result).to.deep.equal({
				templateType:'TF',
				templateVersion: 0.12,
				templateParameters:{variableFiles:['scan/rich-value-types/expressions/variables.tf'],
					variables:{check:'public-read-write',varName2:'varValue2'}}
				});
		})
	})

	  it('resolves with t011 config file', () => {
		return templatereader.readTemplateConfigFromRepo(fs.readFileSync(path.resolve(__dirname, "../config-files/t011.yml"),'utf-8')).then(result => {
			expect(result).to.deep.equal( {
			templateType:'TF',
			templateVersion: 0.11,
			templateParameters:{variableFiles:['abc','tyt'],
			variables:{check:'public-read-write',varName2:'varValue2'}},
			tags:{abc:'xyz',pqw:'oiu'}}
		  )
		})
	  })

	it('resolves with t011 config file without version', () => {
		return templatereader.readTemplateConfigFromRepo(fs.readFileSync(path.resolve(__dirname, "../config-files/t011_withoutVersion.yml"),'utf-8')).then(result => {
			expect(result).to.deep.equal( {
				templateType:'TF',
				templateVersion: 0.11,
				templateParameters:{variableFiles:['abc','tyt'],
					variables:{check:'public-read-write',varName2:'varValue2'}},
				tags:{abc:'xyz',pqw:'oiu'}}
			)
		})
	})

	  it('resolves with templateParameters config file', () => {
		return templatereader.readTemplateConfigFromRepo(fs.readFileSync(path.resolve(__dirname, "../config-files/templateParameters.yml"),'utf-8')).then(result => {
			expect(result).to.deep.equal({
			templateType:'TF',
			templateVersion: 0.13,
			templateParameters:{variableFiles:['abc','tyt'],
			variables:{check:'public-read-write',varName2:'varValue2'},
			policyIdFilters: ['abc','pop'],
			newProp1:['abc'],
			newProp2:'qwerty'},
			tags:{tag1:'value1',tag2:'value2'}}
		  )
		})
	  })

	  it('resolves with CFT config file', () => {
		return templatereader.readTemplateConfigFromRepo(fs.readFileSync(path.resolve(__dirname, "../config-files/cft.yml"),'utf-8')).then(result => {
			expect(result).to.deep.equal({
			templateType:'CFT',
			templateParameters:{variableFiles:['abc','tyt'],
			variables:{check:'public-read-write',varName2:'varValue2'},
			},
			tags:{abc:'xyz',pqw:'oiu'}}
		  )
		})
	  })
})
