import {expect} from 'chai'
import * as ss from '../../src/service/ScanStatus'
import * as sss from '../../src/service/ScanStatusService'

import * as nock from 'nock'
import { PluginType, PrismaCloudConfig } from '../../src/types'
const url = 'https://api.test.com'
const username = 'user'
const password = 'pass'
const token = 'eyJhbGciOiJIUzI1NiJ9.eyJmaXJzdExvZ2luIjpmYWxzZSwiaXNzIjoiaHR0cHM6Ly9hcGlrOHN0YWdlLms4cy5wcmlzbWFjbG91ZC5pbyIsInJlc3RyaWN0IjowLCJpc0FjY2Vzc0tleUxvZ2luIjp0cnVlLCJ1c2VyUm9sZVR5cGVOYW1lIjoiU3lzdGVtIEFkbWluIiwiaXNTU09TZXNzaW9uIjpmYWxzZSwibGFzdExvZ2luVGltZSI6MTYwNzYyMzcwMTY1MywidXNlclJvbGVUeXBlSWQiOjEsInNlbGVjdGVkQ3VzdG9tZXJOYW1lIjoiUEFOVy1kZXYiLCJzZXNzaW9uVGltZW91dCI6MzAwLCJ1c2VyUm9sZUlkIjoiMGY4MDgzZDktNGE0OS00OTNlLWExZTUtMTFmNTU3N2Q3MGE3IiwiaGFzRGVmZW5kZXJQZXJtaXNzaW9ucyI6dHJ1ZSwiZXhwIjoxNjA3OTM0NjY1LCJ1c2VybmFtZSI6Im5pYmFuc2FsQHBhbG9hbHRvbmV0d29ya3MuY29tIiwidXNlclJvbGVOYW1lIjoiU3lzdGVtIEFkbWluIn0.PTk_QIJU3b0RZfQzpR4lsJ_76hqiL7VxosqSUUdaiJE'
const attributes = {
    id :'3fa85f64-5717-4562-b3fc-2c963f66afa6'
}
const successResponse = {
    "data": {
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "attributes": {
        "status": "processing"
      }
    }
  }
 const error = {
    errors: [
      {
        status: "string",
        code: "405",
        detail: "Invalid template file provided",
        source: "Scan-worker-service"
      }
    ]
  }   
describe("tests for getting scan status ", () => {
	let scanStatus : ss.ScanStatus;
	let prismaCloudConfig : PrismaCloudConfig;
	beforeEach(function() {
        scanStatus = new sss.ScanStatusService();
	prismaCloudConfig = new PrismaCloudConfig();
	prismaCloudConfig.setUrl(url);
	prismaCloudConfig.setAccessKey(username);
    prismaCloudConfig.setSecretKey(password);
    prismaCloudConfig.setToken(token);
    prismaCloudConfig.setExpiryTime(Math.round(((new Date()).getTime() / 1000) + 5*60 ));
    });

    it('scan status successfully', () => {
    console.log('nikhil'+ nock.activeMocks())
		nock(url)
  		.get('/iac/v2/scans/'+ attributes.id +'/status')
          .reply(200, successResponse)
          
		return scanStatus.getScanStatus(attributes, prismaCloudConfig, PluginType.GITHUB_APP,{'User-Agent':'Github App/2.0.0'}).then(result => {
			expect (result).to.deep.equal(successResponse)
		})
})

        it('Scan init throws 401', () => {
            nock(url)
            .get('/iac/v2/scans/'+ attributes.id+'/status')
            .reply(401, error)
            
            scanStatus.getScanStatus(attributes, prismaCloudConfig, PluginType.GITHUB_APP,{'User-Agent':'Github App/2.0.0'})
            .catch(function(m) { 
                expect(m.code).to.equal(401)
            })
            
        })
})