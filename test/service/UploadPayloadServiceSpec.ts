import {expect} from 'chai'
import * as ziputil from '../../src/util/ZipUtil'
import * as path from 'path'

import * as nock from 'nock'
import { PluginType, PrismaCloudConfig } from '../../src/types'
import { UploadPayloadService } from '../../src/service/UploadPayloadService'
const url = 'https://s3.amazonaws.com/s3sign2-bucket-hchq3nwuo8ns/s3-sign-demo.json?X-Amz-Security-Token=FQ'

describe("upload file", () => {
	let uploadPayload = new UploadPayloadService();
    let content = ziputil.getZippedFolderArchive(path.resolve(__dirname, "../zipfolder"), path.resolve(__dirname, "/repo.zip"))
    it('uplaods successfully', () => {
		nock('https://s3.amazonaws.com')
            .put('/s3sign2-bucket-hchq3nwuo8ns/s3-sign-demo.json?X-Amz-Security-Token=FQ')
          .reply(200, {})
          
		return uploadPayload.uploadPayload(url, content,{'User-Agent':'Github App/2.0.0'},PluginType.GITHUB_APP)
		.then(result => {expect(result).to.be.equal("{}")})
})

it('throws Internal server error', () => {
	nock('https://s3.amazonaws.com')
		.put('/s3sign2-bucket-hchq3nwuo8ns/s3-sign-demo.json?X-Amz-Security-Token=FQ')
	  .reply(500, {})
	  
	return uploadPayload.uploadPayload(url, content,{'User-Agent':'Github App/2.0.0'},PluginType.GITHUB_APP)
	.then()
	.catch(result => {expect(result.code).to.equal(500)})
})
})