import {expect} from 'chai'
import * as ff from '../../src/util/FileFilter'
import * as nock from 'nock'



describe("filter files", () => {

    it('filter files default pattern', async () => {
        let results =  await ff.filterfiles('test/util/filterfilestest/defaultIncludeExcludePatterns');
        expect(results.length).to.be.equal(8)
    })

    it('filter files with include and exclude pattern', async () => {
        let results =  await ff.filterfiles('test/util/filterfilestest/defaultIncludeExcludePatterns',['abc.csv','test/try.txt'],['a.sh']);
        expect(results.length).to.be.equal(9)
    })

    it('filter files for terraform folder', async () => {
        let results = await ff.getTerraformFolders('test/util/filterfilestest/defaultIncludeExcludePatterns');
        expect(results.length).to.be.equal(2)
    })

})