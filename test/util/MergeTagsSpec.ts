import {expect} from 'chai'
import * as mt from '../../src/util/MergeTags'
import * as nock from 'nock'



describe("merge tags", () => {

    it('merges tags', () => {
        let results =  mt.mergeTags(['key1:value1','key2:value2'] , {'key1':'value1_1', 'key3':'value3'}, 'key3:value3_3,key4:value4');
        expect(results).to.deep.equal({
              'key1' : 'value1_1',
                 'key2' : 'value2',
                'key3' :'value3_3',
                 'key4' : 'value4'
             })
        })

    it('merges tags even if undefined is passed', () => {
        let results =  mt.mergeTags(['key1:value1','key2:value2'] , undefined, '');
        expect(results).to.deep.equal({
            'key1' : 'value1',
            'key2' : 'value2'
        })
    })

})